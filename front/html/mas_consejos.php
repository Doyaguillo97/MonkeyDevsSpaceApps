<!DOCTYPE html>
<html lang="es">

<head>
    <title> MonkeyOnFire </title>
    <link rel="shortcut icon" type="image/x-icon" href="#">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../estilos/estilos.css">
</head>

<body>
    <div class="cuerpo2">
        <!-- CABECERA, TITULO Y ICONO-->
        <div class="cabecera">
            <img src="../imagenes/icono.png" title="MonkeyOnFire" />
            <h2>MonkeyOnFire</h2>
        </div>
        <h1>¿Qué hago frente a un incendio?</h1>
        <h2>En casa</h2>
        <ol>
            <li>Confínese.</li>
            <li>Cierre la puerta, las ventanas y las posibles entradas de aire para proteger la vivienda del humo. Procure no salir de casa.</li>
            <li>Vigile toda la casa y en especial el lado por donde llega el fuego.</li>
            <li>Utilice toallas, alfombras o trapos húmedos y las reservas de agua establecidas.</li>
            <li>Apague cualquier chispa o partícula encendida. Revise los sitios en los que no haya nadie por si hubiera entrado alguna chispa o partícula encendida.</li>
            <li>Escuche la radio</li>
            <li>No utilice el teléfono, si no es para informar a las autoridades. Puede colapsar las líneas.</li>
            <li>Responda a los avisos de ayuda de Bomberos, Protección Civil, Mossos d'Esquadra, Guardia Urbana, etc.</li>
            <li>Infórmese del plan de autoprotección de su urbanización.</li>
        </ol>
        <h2>En el bosque</h2>
        <ol>
            <li>Alejaros del frente del fuego por los lados en dirección opuesta al viento, si podéis, entrad en zona ya quemada lo más pronto posible. Es vital huir en la dirección opuesta al avance del fuego y del humo</li>
            <li>Si podéis, mojad un pañuelo para taparos la cara y evitar así los efectos nocivos del humo. Respirad haciendo inspiraciones poco profundas y lentas, tomando el aire de cerca de tierra y evitando inhalar el humo espeso. Si estáis cerca del mar o de ríos , acercaos al agua y si es necesario, meteos dentro</li>
            <li>No os refugiéis en pozos ni en cuevas, ya que el oxígeno puede terminarse rápidamente. Evitad las zonas con pendiente, los puertos de montaña y los valles estrechos, porque el aire caliente tiende a subir</li>
            <li>Si estáis en el coche, deteneos en un lugar protegido, cerrad las puertas y las ventanas, parad la ventilación del coche y encended los faros para que os puedan encontrar en medio del humo</li>
            <li>Llamad inmediatamente al teléfono de emergencias 112 y a la Policía Municipal 092. La rapidez en que los aviséis puede ser determinante</li>
        </ol>

        <a href="index.php"><img src="../imagenes/atras.png" width="60"/></a>


    </div>

</body>
